package com.rafly.user.rafly_1202160009_si4001_pab_modul2;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, DatePickerDialog.OnDateSetListener, View.OnClickListener {
    private TextView saldo, Date, Time;
    private int jmlsaldo = -1;
    private int minimal_saldo = 50000;
    private int tujuan = 0;
    private Button btn_Date, btn_Time;
    private int mMinute, mHour, mDay, mMonth, mYear;
    private Switch switch_qq;
    private String date, item;
    private int day, month, years;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("MainActivity-onCreate", "silahkan mulai..");
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        saldo = findViewById(R.id.saldo_anda_txt);
        jmlsaldo = Integer.parseInt(saldo.getText().toString());
        if (jmlsaldo < minimal_saldo) {
            saldo.setTextColor(Color.RED);
        }

        Spinner tujuannya = findViewById(R.id.t_spinner);
        if (tujuannya != null) {
            tujuannya.setOnItemSelectedListener(this);
        }
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.tujuan_array, android.R.layout.simple_spinner_item);
        if (tujuannya != null) {
            tujuannya.setAdapter(adapter);
        }


        btn_Date = findViewById(R.id.tgl_btn);
        btn_Time = findViewById(R.id.wkt_btn);
        Date = findViewById(R.id.date);
        Time = findViewById(R.id.time);
        btn_Date.setOnClickListener(this);
        btn_Time.setOnClickListener(this);
        //Switch
        switch_qq = findViewById(R.id.qq_switch);
        Log.d("MainActivity-onCreate", "selesai euy..");
    }

    public void topup(View view) {
        Log.d("MainActivity-topup", "ready top_up!");
        LayoutInflater linf = LayoutInflater.from(this);
        final View inflator = linf.inflate(R.layout.dialog_topup, null);
        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle("Topup");
        alert.setMessage("Masukkan nominal saldo");
        alert.setView(inflator);

        final EditText et1 = inflator.findViewById(R.id.pup_tup);

        alert.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton)
            {
                String s1=et1.getText().toString();
                Log.d("Log", s1);
                jmlsaldo = jmlsaldo + Integer.parseInt(s1);
                saldo.setText(Integer.toString(jmlsaldo));
                if (jmlsaldo > minimal_saldo) {
                    saldo.setTextColor(Color.BLACK);
                }
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.cancel();
            }
        });

        alert.show();
        Log.d("MainActivity-Topup", "berhasil nambah top up gan..");
    }


    public void beliTiket(View view) {
        Log.d("MainActivity-beliTiket", "beli dulu tiket gan!");
        EditText jml = findViewById(R.id.jumlah_tiket);
        int jumlahtiket = Integer.parseInt(jml.getText().toString());
        int biayatravel = tujuan * jumlahtiket;
        if (switch_qq.isChecked()) {
            biayatravel = biayatravel * 2;
        }
        if (jmlsaldo < biayatravel) {
            Toast.makeText(this, "Saldo menyedihkan!", Toast.LENGTH_LONG)
                    .show();
        } else {
            try {
                Intent summary = new Intent(this, Summary.class);
                Bundle bundle = new Bundle();
                bundle.putInt("tujuan", tujuan);
                bundle.putInt("jam", mHour);
                bundle.putString("tujuannya", item);
                bundle.putInt("menit", mMinute);
                bundle.putBoolean("switche", switch_qq.isChecked());
                if (switch_qq.isChecked()) {
                    bundle.putString("tanggalpulang", (day + 1) + "/" + (month) + "/" + years);
                }
                bundle.putInt("biaya", biayatravel);
                bundle.putInt("jmlTiket", jumlahtiket);
                bundle.putString("tanggal", date);
                Log.d("MainActivity-Bundle", "Bundle created!");
                summary.putExtras(bundle);
                Log.d("MainActivity-Bundle", "Bundle inserted!");
                startActivityForResult(summary, 1);
            } catch (Exception e) {
                Toast.makeText(this, "Error! "+e, Toast.LENGTH_LONG)
                        .show();
            }
        }
        Log.d("MainActivity-beliTiket", "beres gan beli tiket nya!");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        Log.d("MainActivity-Result", "mulai hasilnya dong");
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if(resultCode == Activity.RESULT_OK){
                String result=data.getStringExtra("newsaldo");
                jmlsaldo = jmlsaldo - Integer.parseInt(result);
                Log.d("onActivityResult", Integer.toString(jmlsaldo));
                saldo.setText(Integer.toString(jmlsaldo));
                if (jmlsaldo > minimal_saldo) {
                    saldo.setTextColor(Color.BLACK);
                } else {
                    saldo.setTextColor(Color.RED);
                }
            }

        }
        Log.d("MainActivity-Result", "selesai hasil terlihat");
    }

    //Ini digunakan untuk spinner
    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        String spinnerCurrent = adapterView.getItemAtPosition(i).toString();
        item = adapterView.getItemAtPosition(i).toString();
        switch(i) {
            case 0:
                tujuan = 85000;
                Log.d("Spinner", Integer.toString(tujuan));
                break;
            case 1:
                tujuan = 150000;
                Log.d("Spinner", Integer.toString(tujuan));
                break;
            case 2:
                tujuan = 70000;
                Log.d("Spinner", Integer.toString(tujuan));
                break;
            default:
                Log.d("Spinner", "pilihlah bebas");
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        Log.d("MainActivity-spinner", "pilihlah please");
        //Tidak berguna karena default tujuan sudah ditentukan, namun dibutuhkan AdapterView
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Log.d("MainActivity-onDateSet", "pokoknya tentang tanggal");
    }

    @Override
    public void onClick(View v) {
        if (v == btn_Date) {

            // Mengambil tanggal
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);


            DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {

                            Date.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                            date = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
                            Log.d("MainActivity", date);
                            day = dayOfMonth;
                            month = monthOfYear+1;
                            years = year;
                        }
                    }, mYear, mMonth, mDay);
            datePickerDialog.show();
        }
        if (v == btn_Time) {

            // Mengambil waktu saat ini:
            final Calendar c = Calendar.getInstance();
            mHour = c.get(Calendar.HOUR_OF_DAY);
            mMinute = c.get(Calendar.MINUTE);

            //Meluncurkan dialog TimePicker
            TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                    new TimePickerDialog.OnTimeSetListener() {

                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay,
                                              int minute) {

                            Time.setText(hourOfDay + ":" + minute);
                        }
                    }, mHour, mMinute, false);
            timePickerDialog.show();
        }
    }
}
