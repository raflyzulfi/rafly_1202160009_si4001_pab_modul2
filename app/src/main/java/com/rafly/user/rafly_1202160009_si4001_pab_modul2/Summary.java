package com.rafly.user.rafly_1202160009_si4001_pab_modul2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class Summary extends AppCompatActivity {
    private TextView tujuaned, berangkated, pulanged, jumlahtiketed, biayaed;
    private int tujuan, jam, menit, biaya, jmlTiket;
    private String tanggalpulang, tanggal = "tanggal", nama_tujuan;
    private String[] tujuansemua = {"Jakarta", "Cirebon", "Bekasi"};
    private boolean switch1 = false;


    @Override
    protected void onCreate(Bundle savedInstanceBundle) {
        setTheme(R.style.AppTheme);
        Log.d("Summary", "onCreate di load, bosss!!");
        Intent intent = getIntent();
        Bundle sum = intent.getExtras();
        super.onCreate(savedInstanceBundle);
        setContentView(R.layout.summary);
        Log.d("Summary - Bundle", "Bundle received!");
        if (sum != null) {
            tujuan = sum.getInt("tujuan", 0);
            jmlTiket = sum.getInt("jmlTiket", 0);
            jam = sum.getInt("jam", 0);
            menit = sum.getInt("menit", 0);
            biaya = sum.getInt("biaya", 0);
            nama_tujuan = sum.getString("tujuannya", "null");
            switch1 = sum.getBoolean("switche", false);
            if (switch1) {
                tanggalpulang = sum.getString("tanggalpulang", "null");
            }
            tanggal = sum.getString("tanggal", "null");
            Log.d("Summary - Bundle", "Bundle opened!");
        }
        //Mengatur nama tujuan
        tujuaned = findViewById(R.id.tujuan_ed);
        tujuaned.setText(nama_tujuan);
        //Mengatur tanggal
        berangkated = findViewById(R.id.berangkat_ed);
        berangkated.setText(tanggal);
        if (switch1) {
            pulanged = findViewById(R.id.pulang_ed);
            pulanged.setText(tanggalpulang);
        } else {
            pulanged = findViewById(R.id.pulang_ed);
            TextView txtpulang = findViewById(R.id.txt_pulang);
            pulanged.setVisibility(View.GONE);
            txtpulang.setVisibility(View.GONE);
        }

        //Mengatur jumlah tiket
        jumlahtiketed = findViewById(R.id.jmltiket_ed);
        jumlahtiketed.setText(Integer.toString(jmlTiket));

        //Mengatur total biaya
        biayaed = findViewById(R.id.biaya_ed);
        biayaed.setText("harga total:" + Integer.toString(biaya));
        Log.d("Summary-onCreate", "...wait, ini masih di onCreate???");
    }

    public void konfirm(View view) {
        Log.d("Summary-konfirm", "konfirm... mulai!!!");
        Toast.makeText(this, "terimakasih atas pembelian anda", Toast.LENGTH_LONG)
                .show();
        Intent pulangkanlah = new Intent();
        pulangkanlah.putExtra("newsaldo", Integer.toString(biaya));
        setResult(Activity.RESULT_OK, pulangkanlah);
        Log.d("Summary-konfirm", "Finish! " +
                "Saatnya ngerjain KSI, ManLay, Android kelas, " +
                "pricelist kantor, website, dan tugas menumpuk lainnya~" +
                "#tidakngeluhkok");
        finish();
    }
}
